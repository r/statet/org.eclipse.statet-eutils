/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.eutils.autonature;

import org.osgi.framework.BundleContext;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.IPreferenceChangeListener;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.PreferenceChangeEvent;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.ui.plugin.AbstractUIPlugin;


/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin implements IPreferenceChangeListener {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.eutils.autonature"; //$NON-NLS-1$
	
	public static final String ENABLED_PREF_KEY= "enabled"; //$NON-NLS-1$
	
	
	private static Activator instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static Activator getInstance() {
		return instance;
	}
	
	public static void log(final IStatus status) {
		final Activator plugin= getInstance();
		if (plugin != null) {
			final ILog log= plugin.getLog();
			if (log != null) {
				log.log(status);
			}
		}
	}
	
	
	private ConfigManager configManager;
	private ResourceListener listener;
	
	
	public Activator() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		this.configManager= new ConfigManager();
		
		InstanceScope.INSTANCE.getNode(BUNDLE_ID).addPreferenceChangeListener(this);
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		instance= null;
		super.stop(context);
	}
	
	
	public void runStartup() {
		updateMonitor(true);
	}
	
	@Override
	public void preferenceChange(final PreferenceChangeEvent event) {
		if (event.getKey().equals(ENABLED_PREF_KEY)) {
			updateMonitor(false);
		}
	}
	
	public ConfigManager getConfigManager() {
		return this.configManager;
	}
	
	private void updateMonitor(final boolean startup) {
		final IWorkspace workspace= ResourcesPlugin.getWorkspace();
		final boolean enabled= Platform.getPreferencesService().getBoolean(BUNDLE_ID, ENABLED_PREF_KEY, true, null);
		synchronized (this) {
			if (enabled) {
				if (this.listener == null) {
					this.listener= new ResourceListener(this.configManager);
				}
				workspace.addResourceChangeListener(this.listener, IResourceChangeEvent.POST_CHANGE);
				// TODO use save participant
				// if (startup) {
				// }
			}
			else {
				if (this.listener != null) {
					workspace.removeResourceChangeListener(this.listener);
				}
			}
		}
	}
}
