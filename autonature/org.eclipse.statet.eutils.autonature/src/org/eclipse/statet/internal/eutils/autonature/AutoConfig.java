/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.eutils.autonature;

import java.util.Collections;
import java.util.List;


public abstract class AutoConfig {
	
	
	static class Dummy extends AutoConfig {
		
		public Dummy(final String id) {
			super(id, Collections.<Task>emptyList());
		}
		
		@Override
		public String getLabel() {
			return null;
		}
		
	}
	
	
	private final String id;
	
	private final String enabledPrefKey;
	
	private final List<Task> tasks;
	
	
	public AutoConfig(final String id, final List<Task> tasks) {
		this.id= id;
		this.enabledPrefKey= id + ".enabled";
		this.tasks= tasks;
	}
	
	
	public String getId() {
		return this.id;
	}
	
	public String getEnabledPrefKey() {
		return this.enabledPrefKey;
	}
	
	public boolean isAvailable() {
		if (getLabel() == null) {
			return false;
		}
		for (final Task task : this.tasks) {
			if (!task.isAvailable()) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isSupported(final byte mode) {
		for (final Task task : this.tasks) {
			if (!task.isSupported(mode)) {
				return false;
			}
		}
		return true;
	}
	
	public abstract String getLabel();
	
	public List<Task> getTasks() {
		return this.tasks;
	}
	
	
	@Override
	public String toString() {
		return this.id;
	}
	
}
