/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.eutils.autonature;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.SubMonitor;


public abstract class Task {
	
	
	public static final int CONTENT_MATCH= 1;
	
	
	/**
	 * Returns the label of the task (appears in UI).
	 * 
	 * @return the label
	 */
	public abstract String getLabel();
	
	/**
	 * Returns if all requirements (like dependencies, project natures, ...) are available.
	 * 
	 * @return if available
	 */
	public abstract boolean isAvailable();
	
	public boolean isSupported(final byte mode) {
		return true;
	}
	
	/**
	 * Validates if task is required for the specified project.
	 * 
	 * @param project
	 * @param flags TODO
	 * @param m progress monitor
	 * @return
	 * @throws CoreException 
	 */
	public abstract byte check(IProject project,
			int flags, SubMonitor m) throws CoreException;
	
}
