/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.eutils.autorun.nostart;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.ui.IStartup;

import org.eclipse.statet.internal.eutils.autorun.Activator;
import org.eclipse.statet.internal.eutils.autorun.AutoRunner;


public class AutoRunStartup implements IStartup {
	
	
	public AutoRunStartup() {
	}
	
	
	@Override
	public void earlyStartup() {
		if (Boolean.parseBoolean(System.getProperty("org.eclipse.statet.eutils.autorun.disable"))) { //$NON-NLS-1$
			return;
		}
		final IPreferencesService preferences= Platform.getPreferencesService();
		if (preferences.getBoolean(Activator.BUNDLE_ID, Activator.ENABLED_PREF_KEY, true, null)) {
			final String key= preferences.getString(Activator.BUNDLE_ID, Activator.LAUNCH_CONFIG_ID_PREF_KEY, null, null);
			if (key != null) {
				final String mode= preferences.getString(Activator.BUNDLE_ID, Activator.LAUNCH_MODE_ID_PREF_KEY, ILaunchManager.RUN_MODE, null);
				new AutoRunner(key, mode).schedule(500);
			}
		}
	}
	
}
