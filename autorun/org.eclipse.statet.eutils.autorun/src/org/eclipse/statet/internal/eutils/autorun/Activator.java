/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.eutils.autorun;

import org.osgi.framework.BundleContext;

import org.eclipse.ui.plugin.AbstractUIPlugin;


/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.eutils.autorun"; //$NON-NLS-1$
	
	public static final String ENABLED_PREF_KEY= "enabled"; //$NON-NLS-1$
	public static final String LAUNCH_CONFIG_ID_PREF_KEY= "config.id"; //$NON-NLS-1$
	public static final String LAUNCH_MODE_ID_PREF_KEY= "mode.id"; //$NON-NLS-1$
	
	
	private static Activator instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static Activator getInstance() {
		return instance;
	}
	
	
	public Activator() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		instance= null;
		super.stop(context);
	}
	
}
